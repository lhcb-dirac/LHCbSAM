%define site org.lhcb
%define dir %{_libexecdir}/grid-monitoring/probes/%{site}
%define dir2 /usr/lib/ncgx/x_plugins
#%define dir3 /etc/ncgx/metrics.d

%define debug_package %{nil}

Summary: WLCG Compliant Probes from %{site}
Name: nagios-plugins-wlcg-org.lhcb
Version: %(echo ${VERSION:-0.3.24})
Release: 1%{?dist}

License: ASL 2.0
Group: Network/Monitoring
Source0: %{name}-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
#Requires:
AutoReqProv: no
BuildArch: noarch
BuildRequires: python >= 2.4

%description
IT/SAM nagios probes specific for the LHCb VO

%prep
%setup -q

%build

%install
export DONT_STRIP=1
%{__rm} -rf %{buildroot}
install --directory %{buildroot}%{dir}
install --directory %{buildroot}%{dir2}
#install --directory %{buildroot}%{dir3}

%{__cp} -rpf .%dir/wnjob                    %{buildroot}%{dir}
%{__cp} -rpf .%dir/check_pilot_results      %{buildroot}%{dir}
%{__cp} -rpf .%dir/srmvometrics.py          %{buildroot}%{dir}
%{__cp} -rpf .%dir2/lhcb_vofeed.py          %{buildroot}%{dir2}
#%{__cp} -rpf .%dir2/lhcb_webdav.py          %{buildroot}%{dir2}
#%{__cp} -rpf .%dir3/extra_lhcb.cfg          %{buildroot}%{dir3}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{dir}/wnjob
%{dir}/check_pilot_results
%{dir}/srmvometrics.py
%{dir}/srmvometrics.pyc
%{dir}/srmvometrics.pyo
%{dir2}/lhcb_vofeed.py
%{dir2}/lhcb_vofeed.pyc
%{dir2}/lhcb_vofeed.pyo
#%{dir2}/lhcb_webdav.py
#%{dir2}/lhcb_webdav.pyc
#%{dir2}/lhcb_webdav.pyo
#%{dir3}/extra_lhcb.cfg

%changelog
* Mon Dec 12 2016 Andrew McNab <andrew.mcnab@cern.ch> - 0.3.13-1
- Add Pilot/WN-vo-id-card probe

* Sat Dec 10 2016 Andrew McNab <andrew.mcnab@cern.ch> - 0.3.11-1
- Add /etc/ncgx/metrics.d/extra_lhcb.cfg
- Add org.lhcb.Pilot-xx checks for Vac/Vcycle to lhcb-vofeed.py

* Tue Nov 22 2016 Andrew McNab <andrew.mcnab@cern.ch> - 0.3.7-1
- Added check_lhcb_pilot_results
- Remove LFC-probe and SRM-probe

* Thu Sep 22 2016 Marian Babik <marian.babik@cern.ch> - 0.3.7-1
- Added support for HTCondor-CE
- Added support for hostgroups/notifications

* Tue May 9 2016 Marian Babik <marian.babik@cern.ch> - 0.3.6-3
- New MJF probe
- Added ETF configuration

* Mon Jul 6 2009 C. Triantafyllidis <ctria@grid.auth.gr> - 0.1.0-1
- Initial build

